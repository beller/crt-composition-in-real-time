**Patch philosophy**

CRT is a Max patch, running best on the most recent version of Max 7 (7.3.4). The patch is saved in the form of a maxproject, so all necessary files are included.

CRT provides a basic architecture and a set of simple modules for audio input/output, soundfile playback, delay, amplitude LFO, filters, frequency shifters, harmonizers, reverbs, some basic FFT synthesis and a granular motor – all passing through a powerful centralized audio matrix allowing interconnections among all modules (including a module back to itself). All modules contain a switch parameter (sw) which mutes the module when off so that only active modules utilize CPU.

All module parameters are a part of a message-based system for creating events, storing the parameters of all active (sw = 1) modules in a “traditional” but very effective qlist text file. Once an event is created and filled with an initial parameter state, it can be modified to include time-variant values (ramp functions with line or line~) or time-delayed messages forming a sequence within the same event.

CRT is not intended to include EVERYTHING you might need for a real-time computer environment, but to provide a solid framework to build on. The user is expected to be familiar with Max which she/he will need in order to make more interesting interactive relationships between the instruments and the electronics. For example, the user might program an algorithmic player in order to modify existing CRT parameters, or synthesized or sampled sounds. A user may want to incorporate a custom audio processing patch, which they can easily tie into CRT by using the auxiliary send/receive in order to benefit from the central matrix and spatialization.

Every module has access to spatialization including reverb, configurable from 4 to 8 loudspeakers. These are provided by the use of Ircam objects spat.pan and ircamverb~. All positions are determined by azimuth and distance independent of speaker configuration. This means that one can easily work in a 4-speaker setup and then move to 7 or 8 speaker configurations for the concert.

The patch contains a fixed number of each module, but it is relatively simple to add more of a certain module, or else to delete unused ones to lighten the patch.

Since working with CRT involves modifying the contents of the maxproject folder, always keep an unmodified copy of the CRT project folder at hand in order to start a new project.

**Patch by Tom Mays**

(contact@tommays.net) 
2015-2019

Originally made available to composition students at the Strasbourg Conservatory, and to students of the 2018 Academy of Composition – Philippe Manoury / Musica, Strasbourg.

Runs in Max8 on Mac OSX and on Windows. See Cycling74 website (https://cycling74.com) for minimal hardware and OS requirements.

Includes some external objects from IRCAM, Paris – including elements of Spat (ircamverb, spat.pan, spat.viewer) and Max Sound Box (psych~, psychoirtrist~, yin~).

Also, makes use of software developed at the Center for New Music and Audio Technologies (CNMAT) at the University of California, Berkeley (list-interpolate); and from the PeRColate collection by Dan Trueman, modified by Ivica Ico Bukvic et Ji- Sun Kim (munger~).

Special thanks to composers Philippe Manoury and Daniel D'Adamo for suggestions and feedback. Current CRT version 0.21, 14 August 2019