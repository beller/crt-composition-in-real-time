{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 9,
			"architecture" : "x64"
		}
,
		"rect" : [ 100.0, 100.0, 901.0, 750.0 ],
		"bgcolor" : [ 0.9, 0.9, 0.9, 1.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 11.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 10.0, 10.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"showrootpatcherontab" : 0,
		"showontab" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"id" : "obj-2",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 90.0, 110.0, 171.0, 31.0 ],
					"saved_object_attributes" : 					{
						"filename" : "spat.helpstarter.js",
						"parameter_enable" : 0
					}
,
					"text" : "js spat.helpstarter.js spat.viewer.embedded"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 9,
							"architecture" : "x64"
						}
,
						"rect" : [ 100.0, 126.0, 901.0, 724.0 ],
						"bgcolor" : [ 0.9, 0.9, 0.9, 0.9 ],
						"bglocked" : 1,
						"openinpresentation" : 0,
						"default_fontsize" : 11.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 10.0, 10.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"showontab" : 1,
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 230.0, 160.0, 67.0, 19.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 250.0, 29.0, 19.0 ],
									"text" : "thru"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "source" ],
									"patching_rect" : [ 50.0, 590.0, 29.0, 19.0 ],
									"text" : "thru"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-157",
									"maxclass" : "live.menu",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "float" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 230.0, 190.0, 70.0, 15.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "live.dial[38]",
											"parameter_shortname" : "xoffset",
											"parameter_type" : 2,
											"parameter_enum" : [ "single", "leftright", "topbottom" ]
										}

									}
,
									"varname" : "live.menu[1]"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-158",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 230.0, 218.5, 55.0, 17.0 ],
									"text" : "layout $1"
								}

							}
, 							{
								"box" : 								{
									"bubble" : 1,
									"bubbleside" : 2,
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"frgb" : 0.0,
									"id" : "obj-18",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 650.5, 491.0, 191.0, 38.0 ],
									"text" : "see spat.viewer help patch for details"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 700.5, 549.0, 50.0, 19.0 ],
									"text" : "pcontrol"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-36",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 700.5, 529.0, 141.0, 17.0 ],
									"text" : "load spat.viewer.maxhelp"
								}

							}
, 							{
								"box" : 								{
									"aperture" : [ 54.768357, 80.0, 80.0 ],
									"aperturecolor" : [ 1.0, 1.0, 1.0, 0.619608 ],
									"areasmonitoring" : [ 0 ],
									"autozoom" : [ 0 ],
									"backgroundcolor" : [ 0.7, 0.7, 0.7, 0.7 ],
									"backgroundimage" : [ "none" ],
									"backgroundimageangle" : [ 0.0 ],
									"backgroundimageopacity" : [ 1.0 ],
									"backgroundimagequality" : [ "medium" ],
									"backgroundimagescale" : [ 1.0 ],
									"backgroundimagexoffset" : [ 0.0 ],
									"backgroundimageyoffset" : [ 0.0 ],
									"circularconstraint" : [ 0 ],
									"defer" : [ 0 ],
									"display" : [ 1 ],
									"format" : [ "aed" ],
									"globalproportion" : [ 0.1 ],
									"gridlines" : [ 3 ],
									"gridmode" : [ "circular" ],
									"gridspacing" : [ 1.0 ],
									"id" : "obj-3",
									"layout" : [ "leftright" ],
									"listenereditable" : [ 0 ],
									"listenerpitch" : [ 0.0 ],
									"listenerposition" : [ 0.0, 0.0, 0.0 ],
									"listenerproportion" : [ 0.15 ],
									"listenerroll" : [ 0.0 ],
									"listeneryaw" : [ 0.0 ],
									"maxclass" : "spat.viewer.embedded",
									"name" : [ "1", "2", "3" ],
									"numanchors" : [ 0 ],
									"numangulardivisions" : [ 8 ],
									"numareas" : [ 0 ],
									"numinlets" : 1,
									"numoutlets" : 7,
									"numsources" : [ 3 ],
									"numspeakers" : [ 4 ],
									"orientationmode" : [ "yawconstraint", "yawconstraint", "yawconstraint" ],
									"outlettype" : [ "source", "speakers", "source", "", "listener", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 280.0, 520.0, 288.0 ],
									"radius" : [ 1.0, 1.0, 1.0 ],
									"radiusconstraint" : [ 0 ],
									"rightclicklock" : [ 0 ],
									"shoeboxcorners" : [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ],
									"showanchors" : [ 0 ],
									"showanchorslabel" : [ 0 ],
									"showangulardivisions" : [ 0 ],
									"showaperture" : [ 1 ],
									"showareas" : [ 0 ],
									"showbackgroundimage" : [ 1 ],
									"showfps" : [ 0 ],
									"showheadphones" : [ 0 ],
									"showlegend" : [ 1 ],
									"showlistener" : [ 1 ],
									"showradius" : [ 0 ],
									"showshoebox" : [ 0 ],
									"showsources" : [ 1 ],
									"showsourceslabel" : [ 1 ],
									"showsourceslevels" : [ 0 ],
									"showspeakers" : [ 1 ],
									"showspeakershull" : [ 0 ],
									"showspeakerslabel" : [ 1 ],
									"showspeakerslevels" : [ 0 ],
									"showspeakersprojection" : [ 0 ],
									"showspeakersradius" : [ 0 ],
									"showspeakerstriangulation" : [ 0 ],
									"showviewer" : [ 1 ],
									"sourcecolor" : [ 0.490196, 1.0, 0.0, 1.0, 0.490196, 1.0, 0.0, 1.0, 0.490196, 1.0, 0.0, 1.0 ],
									"sourceproportion" : [ 0.07 ],
									"sourceseditable" : [ 1 ],
									"sourceslevels" : [ -60.0, -60.0, -60.0 ],
									"sourcespositions" : [ -0.979124, 0.89907, -0.275902, 0.866025, -0.5, 0.0, -0.671425, -0.5, 0.096448 ],
									"speakerseditable" : [ 0 ],
									"speakerslevels" : [ -60.0, -60.0, -60.0, -60.0 ],
									"speakerspositions" : [ -0.707107, 0.707107, 0.0, 0.707107, 0.707107, 0.0, 0.707107, -0.707107, 0.0, -0.707107, -0.707107, 0.0 ],
									"speakersproportion" : [ 0.1 ],
									"useopengl" : [ 0 ],
									"viewpoint" : [ "top" ],
									"xoffset" : [ 0.0 ],
									"yaw" : [ -11.875725, 0.0, 0.0 ],
									"yoffset" : [ 0.0 ],
									"zoffset" : [ 0.0 ],
									"zoom" : [ 0.37574 ],
									"zoomlock" : [ 0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-17",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "float", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 190.0, 50.0, 19.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"frgb" : 0.0,
									"id" : "obj-8",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 310.0, 244.0, 431.0, 31.0 ],
									"text" : "spat.viewer.embedded has the same attributes/messages as spat.viewer but it is directly visible inside the patcher (not in a floating window)."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 619.0, 32.0, 19.0 ],
									"text" : "print"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "bpatcher",
									"name" : "spat.copyright.maxpat",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 20.0, 649.0, 316.0, 60.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.0,
									"id" : "obj-15",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 220.0, 162.0, 17.0 ],
									"text" : "backgroundcolor 0.7 0.7 0.7 $1"
								}

							}
, 							{
								"box" : 								{
									"background" : 1,
									"border" : 0,
									"filename" : "spat.helpdetails.js",
									"id" : "obj-2",
									"ignoreclick" : 1,
									"jsarguments" : [ "spat.viewer.embedded", 125 ],
									"maxclass" : "jsui",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 10.0, 10.0, 750.0, 230.0 ],
									"varname" : "digest_jsui"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-158", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-157", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 239.5, 242.25, 59.5, 242.25 ],
									"source" : [ "obj-158", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 560.5, 578.5, 59.5, 578.5 ],
									"source" : [ "obj-3", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 477.0, 578.5, 59.5, 578.5 ],
									"source" : [ "obj-3", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 393.5, 578.5, 59.5, 578.5 ],
									"source" : [ "obj-3", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 310.0, 578.5, 59.5, 578.5 ],
									"source" : [ "obj-3", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 226.5, 578.5, 59.5, 578.5 ],
									"source" : [ "obj-3", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 143.0, 578.5, 59.5, 578.5 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 59.5, 578.5, 59.5, 578.5 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-157", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 10.0, 110.0, 45.0, 19.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"default_fontsize" : 11.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial",
						"fontsize" : 11.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p basic",
					"varname" : "basic_tab"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 9,
							"architecture" : "x64"
						}
,
						"rect" : [ 0.0, 26.0, 901.0, 724.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 11.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 10.0, 10.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"showontab" : 1,
						"boxes" : [  ],
						"lines" : [  ]
					}
,
					"patching_rect" : [ 300.0, 300.0, 50.0, 19.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"default_fontsize" : 11.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial",
						"fontsize" : 11.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p ?",
					"varname" : "q_tab"
				}

			}
 ],
		"lines" : [  ],
		"parameters" : 		{
			"obj-1::obj-157" : [ "live.dial[38]", "xoffset", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "spat.helpdetails.js",
				"bootpath" : "/Users/tcarpent/forge/spat/Spat4MaxMSP/javascript",
				"patcherrelativepath" : "../javascript",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "spat.copyright.maxpat",
				"bootpath" : "/Users/tcarpent/forge/spat/Spat4MaxMSP/patchers",
				"patcherrelativepath" : "../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "thru.maxpat",
				"bootpath" : "/Applications/Max 6.1/patches/m4l-patches/Pluggo for Live resources/patches",
				"patcherrelativepath" : "../../../../../../Applications/Max 6.1/patches/m4l-patches/Pluggo for Live resources/patches",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "spat.helpstarter.js",
				"bootpath" : "/Users/tcarpent/forge/spat/Spat4MaxMSP/javascript",
				"patcherrelativepath" : "../javascript",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "spat.viewer.embedded.mxo",
				"type" : "iLaX"
			}
 ]
	}

}
